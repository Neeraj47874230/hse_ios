//
//  SuccessPopUPViewController.swift
//  HSE
//
//  Created by Neeraj Tiwari on 24/05/19.
//  Copyright © 2019 Sendan. All rights reserved.
//

import UIKit

class SuccessPopUPViewController: UIViewController {
    @IBAction func okeybtnClicked(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBOutlet weak var okBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.okBtn.backgroundColor = .clear
        self.okBtn.layer.cornerRadius = 25
        self.okBtn.layer.borderWidth = 2
        self.okBtn.layer.borderColor = UIColor.lightGray.cgColor
        // Do any additional setup after loading the view.
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
