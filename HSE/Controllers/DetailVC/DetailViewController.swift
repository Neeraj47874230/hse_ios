//
//  DetailViewController.swift
//  HSE
//
//  Created by Neeraj tiwari on 19/05/19.
//  Copyright © 2019 Sendan. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {

    @IBOutlet weak var pendingBtn: UIButton!
    @IBOutlet weak var tblView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.pendingBtn.backgroundColor = UIColor.darkGray
    registerTableCells()
        // Do any additional setup after loading the view.
    }

    func registerTableCells()
    {
        self.tblView.register(UINib(nibName:"DetailCell",bundle: nil),forCellReuseIdentifier: "DetailCell")
        self.tblView.reloadData()
        
        
    }
    @IBAction func backBtnAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func topBtnTapped(_ sender: UIButton) {
        for item in self.view.subviews {
            if item is UIButton && item.tag > 100
            {
                let btn = item as! UIButton
                if btn.tag == sender.tag
                {
                    btn.backgroundColor = UIColor.darkGray
                }else
                {
                    btn.backgroundColor = UIColor.lightGray

                }
            }
        }
        
        
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    // MARK: - UITableViewMethods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        
        
        return 8;
    }
    
    
    // Row display. Implementers should *always* try to reuse cells by setting each cell's reuseIdentifier and querying for available reusable cells with dequeueReusableCellWithIdentifier:
    // Cell gets various attributes set automatically based on table (separators) and data source (accessory views, editing controls)
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "DetailCell", for: indexPath) as! DetailCell
        
        
        return cell;
        
        
    }
   
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        
        return 60
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = SORViewController(nibName: "SORViewController", bundle: nil)
        self.navigationController?.pushViewController(vc, animated: true)
    }

}
