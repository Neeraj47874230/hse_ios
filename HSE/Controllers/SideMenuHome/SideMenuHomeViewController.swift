//
//  SideMenuHomeViewController.swift
//  HSE
//
//  Created by Neeraj tiwari on 19/05/19.
//  Copyright © 2019 Sendan. All rights reserved.
//

import UIKit

class SideMenuHomeViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    @IBOutlet weak var tblView: UITableView!
    var textArray : [String] = [String]()
    var imageArray : [String] = [String]()
    var bottomTextArray : [String] = [String]()

    
    override func viewDidLoad() {
        super.viewDidLoad()
self.navigationController?.isNavigationBarHidden = true
        initialiseVariables()
        registerTableCells()
        // Do any additional setup after loading the view.
    }
    
    
    // MARK:- UserDefinedFunctions
    func registerTableCells()
    {
    self.tblView.register(UINib(nibName:"SideMenuCell",bundle: nil),forCellReuseIdentifier: "SideMenuCell")
           self.tblView.register(UINib(nibName:"SideBottomCell",bundle: nil),forCellReuseIdentifier: "SideBottomCell")
    }
    func initialiseVariables()
    {
        textArray = ["Home","Safety Observation","Incident Report","Inspection Report","Analytical Report","Audit","OSHAS","Location Set","Contact Us"]
        imageArray = ["side_home","side_safety","side_warning","side_inspection","side_report","side_audit","menu_icon","side_location","menu_icon","menu_icon"]
        
         bottomTextArray = ["About this App","Help And Feedback","Terms Of Use","FAQ"]
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    // MARK: - UITableView Methods

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if (section == 0){
        return textArray.count
        }
        if (section == 3)
        {
            return bottomTextArray.count

        }
        return 1;
    }
    
    
    // Row display. Implementers should *always* try to reuse cells by setting each cell's reuseIdentifier and querying for available reusable cells with dequeueReusableCellWithIdentifier:
    // Cell gets various attributes set automatically based on table (separators) and data source (accessory views, editing controls)
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if (indexPath.section == 0){

          let cell = tableView.dequeueReusableCell(withIdentifier: "SideMenuCell", for: indexPath) as! SideMenuCell
        cell.txtLbl.text = textArray[indexPath.row]
        cell.imgView.image = UIImage.init(named: imageArray[indexPath.row])
        return cell;
        }
        if (indexPath.section == 3)
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SideBottomCell", for: indexPath) as! SideBottomCell
            cell.txtLbl.text = bottomTextArray[indexPath.row]
            return cell;
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SideBottomCell", for: indexPath) as! SideBottomCell
        cell.txtLbl.isHidden = true
        cell.imgView.isHidden = true
        return cell;
        
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 4;
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if (indexPath.section == 0){
            return 55
        }
        if (indexPath.section == 3)
        {
            return 30
            
        }
        return 10;
    }
    
}
