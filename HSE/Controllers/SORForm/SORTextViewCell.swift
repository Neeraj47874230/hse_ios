//
//  SORTextViewCell.swift
//  HSE
//
//  Created by Neeraj tiwari on 19/05/19.
//  Copyright © 2019 Sendan. All rights reserved.
//

import UIKit

class SORTextViewCell: UITableViewCell {

    @IBOutlet weak var topLbl: UILabel!
    @IBOutlet weak var txtView: UITextView!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.txtView.layer.cornerRadius = 8.0
        self.txtView.layer.masksToBounds = true
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
