//
//  SORBottomCell.swift
//  HSE
//
//  Created by Neeraj tiwari on 19/05/19.
//  Copyright © 2019 Sendan. All rights reserved.
//

import UIKit
protocol SORBottomCellDelegate: class {
    func nextClicked()
}

class SORBottomCell: UITableViewCell {
    weak var delegate: SORBottomCellDelegate?

    @IBOutlet weak var nextBtn: UIButton!
    @IBAction func nextBtnClicked(_ sender: Any) {
        self.delegate?.nextClicked()
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
