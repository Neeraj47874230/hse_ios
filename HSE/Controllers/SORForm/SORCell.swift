//
//  SORCell.swift
//  HSE
//
//  Created by Neeraj tiwari on 19/05/19.
//  Copyright © 2019 Sendan. All rights reserved.
//

import UIKit

class SORCell: UITableViewCell {

    @IBOutlet weak var outerView: UIView!
    @IBOutlet weak var txtFld: UITextField!
    @IBOutlet weak var topLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.outerView.layer.cornerRadius = 8.0
        self.outerView.layer.masksToBounds = true
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
