//
//  SORViewController.swift
//  HSE
//
//  Created by Neeraj tiwari on 19/05/19.
//  Copyright © 2019 Sendan. All rights reserved.
//

import UIKit
import PopupDialog


class SORViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,SearchViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,SORImageCellDelegate,SORBottomCellDelegate {
   
    
    var textArray : [String] = [String]()
    var model : SORModel = SORModel()
    @IBOutlet weak var tblView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tblView.estimatedRowHeight = 90;
        initialiseVariables()
registerTableCells()
        // Do any additional setup after loading the view.
    }

    // MARK: - UserDefinedFunctions

    @IBAction func backBtnAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    func registerTableCells()
    {
        self.tblView.register(UINib(nibName:"SORCell",bundle: nil),forCellReuseIdentifier: "SORCell")
         self.tblView.register(UINib(nibName:"SORTextViewCell",bundle: nil),forCellReuseIdentifier: "SORTextViewCell")
        self.tblView.register(UINib(nibName:"SORBottomCell",bundle: nil),forCellReuseIdentifier: "SORBottomCell")
        self.tblView.register(UINib(nibName:"SORImageCell",bundle: nil),forCellReuseIdentifier: "SORImageCell")
       
    }
    func initialiseVariables()
    {
        model.images.append(UIImage.init(named: "Group 77")!)
        textArray = ["Observation Action & Response","Project Details","Location","Category","Observation Details","Photos","Corrective & Prevention Action","Involved Person","Immediate In-charge","Forward To"]
       
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: - UITableViewMethods

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if (section == 1)
        {
            return 1;
        }
        
        return textArray.count;
    }
    
    
    // Row display. Implementers should *always* try to reuse cells by setting each cell's reuseIdentifier and querying for available reusable cells with dequeueReusableCellWithIdentifier:
    // Cell gets various attributes set automatically based on table (separators) and data source (accessory views, editing controls)
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if indexPath.section == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SORBottomCell", for: indexPath) as! SORBottomCell
            
            cell.delegate = self
            return cell;
        }
        
        if indexPath.row == 4 || indexPath.row == 6 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SORTextViewCell", for: indexPath) as! SORTextViewCell
            cell.topLbl.text = textArray[indexPath.row]
            
            return cell;
            
        }
        if indexPath.row == 5 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SORImageCell", for: indexPath) as! SORImageCell
            cell.delegate = self
            cell.imagesArr = model.images
            cell.reloadCollection()
            return cell;
            
        }
            let cell = tableView.dequeueReusableCell(withIdentifier: "SORCell", for: indexPath) as! SORCell
            cell.topLbl.text = textArray[indexPath.row]
        cell.txtFld.placeholder = textArray[indexPath.row]
        cell.txtFld.attributedPlaceholder = NSAttributedString(string: cell.txtFld.placeholder! , attributes: [NSAttributedString.Key.foregroundColor: UIColor(red: 77.0/255.0, green: 120.0/255.0, blue: 148.0/255.0, alpha: 1.0)])
        
        switch indexPath.row {
        case 0:
         cell.txtFld.text =  model.observationStr
        case 1:
           cell.txtFld.text = model.projectStr
        case 2:
           cell.txtFld.text = model.locationStr
        case 3:
          cell.txtFld.text =  model.categoryStr
        case 7:
           cell.txtFld.text = model.personStr
        case 8:
           cell.txtFld.text = model.immediateStr
        case 9:
           cell.txtFld.text = model.forwardStr
            
            
            
            
        default:
           cell.txtFld.text = model.observationStr
        }
        
            return cell;
      
        
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 2;
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if (indexPath.row == 5)
        {
//            let val = CGFloat(model.images.count / 3)
//            let roun = val.roundToInt()
//            return (CGFloat(roun) * 120.0) + 37
//           // return (CGFloat(ceilf(Float(model.images.count / 3)))) * 120 + 37;
//            return 200;
            if model.images.count > 0 && model.images.count < 4
            {
                return 157;
            }
            if model.images.count > 3 && model.images.count < 7
            {
                return 277;
            }
            if model.images.count > 6 && model.images.count < 10
            {
                return 397;
            }
        }
       return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if indexPath.section == 1 {
            return;
        }
        if indexPath.row == 4 || indexPath.row == 5 || indexPath.row == 6 {
            return;
        }
        let vc = SearchViewController(nibName: "SearchViewController", bundle: nil)
        vc.delegate = self
        vc.selectedTag = indexPath.row
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
//    var observationStr : String = ""
//    var projectStr : String = ""
//    var  locationStr: String = ""
//    var categoryStr : String = ""
//    var detailStr : String = ""
//    var correctStr : String = ""
//    var personStr : String = ""
//    var immediateStr : String = ""
//    var forwardStr : String = ""
//    var images : [UIImage] = [UIImage]()
    func searchResultSelected(tag : NSInteger, str : String)
    {
        
        switch tag {
        case 0:
            model.observationStr = str
        case 1:
            model.projectStr = str
        case 2:
            model.locationStr = str
        case 3:
            model.categoryStr = str
        case 7:
            model.personStr = str
        case 8:
            model.immediateStr = str
        case 9:
            model.forwardStr = str
            
            
            
            
        default:
            model.observationStr = str
        }
        self.tblView.reloadData()
        
    }
    
    func addImageTapped()
    {
        let alert:UIAlertController=UIAlertController(title: "Choose Image", message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        let cameraAction = UIAlertAction(title: "Choose existing photo", style: UIAlertAction.Style.default)
        {
            UIAlertAction in
            self.checkLibraryCalling()
        }
        let gallaryAction = UIAlertAction(title: "Take new photo", style: UIAlertAction.Style.default)
        {
            UIAlertAction in
            self.callCamera()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel)
        {
            UIAlertAction in
        }
        
        // Add the actions
        alert.addAction(cameraAction)
        alert.addAction(gallaryAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
    // MARK:checkinPhotosAcess
    func checkLibraryCalling()
    {
        let myPickerController = UIImagePickerController()
        myPickerController.delegate = self;
        myPickerController.sourceType = UIImagePickerController.SourceType.photoLibrary
        
        self.present(myPickerController, animated: true, completion: nil)
        NSLog("gallery");
    }
    
    
    func callCamera(){
        let myPickerController = UIImagePickerController()
        myPickerController.delegate = self;
        myPickerController.sourceType = UIImagePickerController.SourceType.camera
        self.present(myPickerController, animated: true, completion: nil)
        NSLog("Camera");
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any])
    {
        picker .dismiss(animated: true, completion: nil)
        let selectedImage : UIImage = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
        model.images.insert(selectedImage, at: 0)
        self.tblView.reloadData()
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
    {
        self.dismiss(animated: true, completion: nil)

    }
    func nextClicked() {
        self.showCustomDialog()
    }
    func showCustomDialog(animated: Bool = true) {
        
        let failVC = SuccessPopUPViewController(nibName: "SuccessPopUPViewController", bundle: nil)
        let popup = PopupDialog(viewController: failVC,
                                buttonAlignment: .horizontal,
                                transitionStyle: .bounceDown,
                                tapGestureDismissal: true,
                                panGestureDismissal: false)
        present(popup, animated: animated, completion: nil)
    }
}
extension CGFloat {
    func roundToInt() -> Int{
        var value = Int(self)
        var f = self - CGFloat(value)
        if f > 0 {
            return value + 1
        } else {
            return value
        }
    }
}
