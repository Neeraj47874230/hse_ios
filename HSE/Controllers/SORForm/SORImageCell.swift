//
//  SORImageCell.swift
//  HSE
//
//  Created by Neeraj tiwari on 19/05/19.
//  Copyright © 2019 Sendan. All rights reserved.
//

import UIKit

protocol SORImageCellDelegate {
    func addImageTapped()
}

class SORImageCell: UITableViewCell,UICollectionViewDataSource,UICollectionViewDelegate {

    @IBOutlet weak var collectionView: UICollectionView!
    var imagesArr : [UIImage] = [UIImage]()
    var delegate : SORImageCellDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func reloadCollection()
    {
        self.collectionView.register(UINib(nibName:"ImageCollectionCell", bundle: nil), forCellWithReuseIdentifier: "ImageCollectionCell")
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        self.collectionView.collectionViewLayout = layout
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
        self.collectionView.reloadData()
    }
    
    // MARK:- UICollectionViewMethods
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
        
    {
       
        return imagesArr.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCollectionCell", for: indexPath) as! ImageCollectionCell
            cell.imgView.image = imagesArr[indexPath.item]
            return cell;
       
    }
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int
    {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
       if imagesArr.count - indexPath.item == 1
       {
          self.delegate?.addImageTapped()
        }
    }
    
}
extension SORImageCell : UICollectionViewDelegateFlowLayout {
    //1
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
       
        return CGSize(width:(UIScreen.main.bounds.size.width - 62)/3 , height: 120)
        
}
}
