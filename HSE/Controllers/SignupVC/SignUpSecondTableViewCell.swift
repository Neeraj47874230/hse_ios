//
//  SignUpSecondTableViewCell.swift
//  HSE
//
//  Created by Neeraj Tiwari on 14/05/19.
//  Copyright © 2019 Sendan. All rights reserved.
//

import UIKit
protocol SignUpSecondTableViewCellDelegate: class {
    func signupEntryField(signupEntryStr: String, _ sender: Any)
    func ShowPasswordClicked(_ sender: Any)
}
class SignUpSecondTableViewCell: UITableViewCell {
    @IBAction func showHidePasswordClicked(_ sender: Any) {
         self.delegate?.ShowPasswordClicked(self)
    }
    @IBOutlet weak var hideShowPasswordBtn: UIButton!
    @IBAction func allTextFldAction(_ sender: Any) {
        self.delegate?.signupEntryField(signupEntryStr: self.entryTextfield.text!, self)
    }
    weak var delegate: SignUpSecondTableViewCellDelegate?
    @IBOutlet weak var entryTextfield: UITextField!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
