//
//  SignUpThirdTableViewCell.swift
//  HSE
//
//  Created by Neeraj Tiwari on 14/05/19.
//  Copyright © 2019 Sendan. All rights reserved.
//

import UIKit
protocol SignUpThirdTableViewCellDelegate: class {
    func continueClicked()
}

class SignUpThirdTableViewCell: UITableViewCell {
    @IBOutlet weak var countinueBtn: UIButton!
    @IBAction func countinueBtnClicked(_ sender: Any) {
        self.delegate?.continueClicked()
    }
    weak var delegate: SignUpThirdTableViewCellDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
