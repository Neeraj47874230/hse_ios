//
//  SignUPViewController.swift
//  HSE
//
//  Created by Neeraj Tiwari on 14/05/19.
//  Copyright © 2019 Sendan. All rights reserved.
//

import UIKit
import Alamofire
class SignUPViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,SignUpThirdTableViewCellDelegate, SignUpSecondTableViewCellDelegate {
    
    @IBOutlet weak var signUpBtn: UIButton!
    @IBOutlet weak var loginBtn: UIButton!
    @IBOutlet weak var loginLowerLabel: UILabel!
    @IBOutlet weak var SignUpTable: UITableView!
    @IBOutlet weak var signUpLowerLabel: UILabel!
    
    var signupEmployeeCode: String? = ""
    var signupFullName: String? = ""
    var signupPassword: String? = ""
    var signupConfirmPassword: String? = ""
    var signupMobileNumber: String? = ""
    var signupEmail: String? = ""
    var employeeInfoDict : NSDictionary = NSDictionary()
    
    var passHidden : Bool = true
    var confirmPassHidden : Bool = true
    
    override func viewDidLoad() {
        super.viewDidLoad()

        SignUpTable.separatorStyle = UITableViewCell.SeparatorStyle.none
        self.signUpLowerLabel.backgroundColor = UIColor(red: 206.0/255.0, green: 120.0/255.0, blue: 81.0/255.0, alpha: 1.0) ;
        self.loginLowerLabel.isHidden = true ;
        self.signUpBtn.isSelected = true
        self.loginBtn.isSelected = false
        
        
        SignUpTable.register(UINib(nibName: "SignUpFirstTableViewCell", bundle: nil), forCellReuseIdentifier: "SignUpFirstTableViewCell");
        SignUpTable.register(UINib(nibName: "SignUpSecondTableViewCell", bundle: nil), forCellReuseIdentifier: "SignUpSecondTableViewCell");
        SignUpTable.register(UINib(nibName: "SignUpThirdTableViewCell", bundle: nil), forCellReuseIdentifier: "SignUpThirdTableViewCell");
//        let tapGesture = UITapGestureRecognizer(target: self, action: Selector("hideKeyboard"))
//        tapGesture.cancelsTouchesInView = true
//        self.SignUpTable.addGestureRecognizer(tapGesture)
    }

        
//func hideKeyboard() {
//    self.SignUpTable.endEditing(true)
//}
//    func dismissKeyboard() {
//        //Causes the view (or one of its embedded text fields) to resign the first responder status.
//     //   self.SignUpTable.endEditing(true)
//        self.SignUpTable.setEditing(false, animated: true)
//    }
    
    func signupEntryField(signupEntryStr: String, _ sender: Any) {
        //       print(signupEntryStr)
        
        if (sender as AnyObject).tag == 1{
            //           print ("0")
            signupEmployeeCode = signupEntryStr
        }
        else if (sender as AnyObject).tag == 2{
            //           print ("1")
            signupFullName = signupEntryStr
        }
        else if (sender as AnyObject).tag == 3{
            //          print ("2")
           signupPassword = signupEntryStr
            
        }else if (sender as AnyObject).tag == 4{
            //          print ("3")
         signupConfirmPassword = signupEntryStr
            
        }
    }
    func ShowPasswordClicked(_ sender: Any) {
        if (sender as AnyObject).tag == 1 {

               employeeInfoAPI()
        }
        else if (sender as AnyObject).tag == 3 {
            if self.passHidden {
                self.passHidden = false
            }else{
                self.passHidden = true
            }
            let indexPath = IndexPath(item: 3, section: 0)
            SignUpTable.reloadRows(at: [indexPath], with: .top)
        }
        else if (sender as AnyObject).tag == 4 {
            if self.confirmPassHidden {
                self.confirmPassHidden = false
            }else{
                self.confirmPassHidden = true
            }
            let indexPath = IndexPath(item: 4, section: 0)
            SignUpTable.reloadRows(at: [indexPath], with: .top)
        }
    }
    // MARK: - tableViewDelegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SignUpFirstTableViewCell",for: indexPath) as! SignUpFirstTableViewCell
            cell.selectionStyle = .none
            // cell1.delegate=self
            cell.tag=indexPath.row
            
            return cell
        }else if indexPath.row == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SignUpSecondTableViewCell",for: indexPath) as! SignUpSecondTableViewCell
            cell.selectionStyle = .none
            cell.entryTextfield.layer.cornerRadius = 12.0
            cell.entryTextfield.clipsToBounds = true
             cell.entryTextfield.attributedPlaceholder = NSAttributedString(string: "Employee Code", attributes: [NSAttributedString.Key.foregroundColor: UIColor(red: 0.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 1.0)])
            let indentView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 20))
            cell.entryTextfield.leftView = indentView
            cell.entryTextfield.leftViewMode = .always
            
            let indentViewR = UIView(frame: CGRect(x: 0, y: 0, width: 40, height: 20))
            cell.entryTextfield.rightView = indentViewR
            cell.entryTextfield.rightViewMode = .always
            
            cell.entryTextfield.keyboardType = .numberPad
            cell.hideShowPasswordBtn.setImage(UIImage.init(named: "search.png"), for: .normal)
            cell.entryTextfield.text = signupEmployeeCode
            
            cell.delegate=self
            cell.tag=indexPath.row
            return cell
            
        }else if indexPath.row == 2 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SignUpSecondTableViewCell",for: indexPath) as! SignUpSecondTableViewCell
            cell.selectionStyle = .none
            cell.entryTextfield.layer.cornerRadius = 12.0
            cell.entryTextfield.clipsToBounds = true
             cell.entryTextfield.attributedPlaceholder = NSAttributedString(string: "Name", attributes: [NSAttributedString.Key.foregroundColor: UIColor(red: 0.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 1.0)])
            let indentView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 20))
            cell.entryTextfield.leftView = indentView
            cell.entryTextfield.leftViewMode = .always
            
            let indentViewR = UIView(frame: CGRect(x: 0, y: 0, width: 40, height: 20))
            cell.entryTextfield.rightView = indentViewR
            cell.entryTextfield.rightViewMode = .always
            
            cell.entryTextfield.text = self.signupFullName
            cell.entryTextfield.isUserInteractionEnabled = false
            cell.hideShowPasswordBtn.isHidden = true
            
             cell.delegate=self
            cell.tag=indexPath.row
            return cell
        }else if indexPath.row == 3 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SignUpSecondTableViewCell",for: indexPath) as! SignUpSecondTableViewCell
            cell.selectionStyle = .none
            cell.entryTextfield.layer.cornerRadius = 12.0
            cell.entryTextfield.clipsToBounds = true
             cell.entryTextfield.attributedPlaceholder = NSAttributedString(string: "Password", attributes: [NSAttributedString.Key.foregroundColor: UIColor(red: 0.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 1.0)])
            
            let indentView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 20))
             cell.entryTextfield.leftView = indentView
             cell.entryTextfield.leftViewMode = .always
            
            let indentViewR = UIView(frame: CGRect(x: 0, y: 0, width: 40, height: 20))
            cell.entryTextfield.rightView = indentViewR
            cell.entryTextfield.rightViewMode = .always
            
            cell.hideShowPasswordBtn.isHidden = false
            cell.entryTextfield.text = signupPassword
            if self.passHidden {
                cell.hideShowPasswordBtn.isSelected = false;
                cell.entryTextfield.isSecureTextEntry = true;
            } else {
                cell.hideShowPasswordBtn.isSelected = true;
                cell.entryTextfield.isSecureTextEntry = false;
            }
             cell.delegate=self
            cell.tag=indexPath.row
            return cell
            
        }else if indexPath.row == 4 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SignUpSecondTableViewCell",for: indexPath) as! SignUpSecondTableViewCell
            cell.selectionStyle = .none
            cell.entryTextfield.layer.cornerRadius = 12.0
            cell.entryTextfield.clipsToBounds = true
             cell.entryTextfield.attributedPlaceholder = NSAttributedString(string: "Confirm password", attributes: [NSAttributedString.Key.foregroundColor: UIColor(red: 0.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 1.0)])
            let indentView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 20))
            cell.entryTextfield.leftView = indentView
            cell.entryTextfield.leftViewMode = .always
            
            let indentViewR = UIView(frame: CGRect(x: 0, y: 0, width: 40, height: 20))
            cell.entryTextfield.rightView = indentViewR
            cell.entryTextfield.rightViewMode = .always
            
            cell.hideShowPasswordBtn.isHidden = false
            cell.entryTextfield.isSecureTextEntry = true
            cell.entryTextfield.text = signupConfirmPassword
            if self.confirmPassHidden {
                cell.hideShowPasswordBtn.isSelected = false;
                cell.entryTextfield.isSecureTextEntry = true;
            } else {
                cell.hideShowPasswordBtn.isSelected = true;
                cell.entryTextfield.isSecureTextEntry = false;
            }
             cell.delegate=self
            cell.tag=indexPath.row
            return cell
            
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "SignUpThirdTableViewCell",for: indexPath) as! SignUpThirdTableViewCell
            cell.selectionStyle = .none
            cell.countinueBtn.layer.cornerRadius = 12.0
            cell.countinueBtn.clipsToBounds = true
            
           cell.delegate=self
            cell.tag=indexPath.row
            
            return cell
        }
    }
    // method to run when table view cell is tapped
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        
        if indexPath.row == 0 {
            return 100
        }else if indexPath.row == 1 || indexPath.row == 2 || indexPath.row == 3 || indexPath.row == 4  {
            return 81
        } else if indexPath.row == 5 {
            return 97
        }
        return 0
    }
    @IBAction func loginBtnClicked(_ sender: Any) {
        self.loginLowerLabel.backgroundColor = UIColor(red: 206.0/255.0, green: 120.0/255.0, blue: 81.0/255.0, alpha: 1.0) ;
        self.signUpLowerLabel.isHidden = true ;
        self.loginLowerLabel.isHidden = false ;
        self.signUpBtn.isSelected = false
        self.loginBtn.isSelected = true
        
        var isLogin = false
        for item in self.navigationController?.viewControllers as! [UIViewController] {
            if item is LoginViewController{
                isLogin = true
                let loginVc = item as! LoginViewController
                self.navigationController?.popToViewController(loginVc, animated: true)
                break;
            }
        }
        if isLogin == false {
            let vc = LoginViewController(nibName: "LoginViewController", bundle: nil)
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    @IBAction func signUpBtnClicked(_ sender: Any) {
        self.signUpLowerLabel.backgroundColor =  UIColor(red: 206.0/255.0, green: 120.0/255.0, blue: 81.0/255.0, alpha: 1.0) ;
        self.loginLowerLabel.isHidden = true
        self.signUpLowerLabel.isHidden = false ;
        self.signUpBtn.isSelected = true
        self.loginBtn.isSelected = false
        
       
    }
    func continueClicked() {
//        let vc = HomeViewController(nibName: "HomeViewController", bundle: nil)
//        self.navigationController?.pushViewController(vc, animated: true)
        self.signupAPI()
    }
    // MARK: - employeeInfoAPI
    func employeeInfoAPI(){
        self.view.endEditing(true)
        if self.signupEmployeeCode!.isEmpty {
            ModalController.showNegativeCustomAlertWith(title: "Please enter employee code.", msg: "")
            return
        }
        ModalClass.startLoading(self.view)

        let params = ["employee_id":"\(self.signupEmployeeCode!)"]
        print(params)
        Alamofire.request("\(Constant.BASE_URL)Employee_reg_info.get", method:.post, parameters: params, encoding: URLEncoding.default).validate().responseJSON {
            response in
            ModalClass.stopLoading()
            
            switch response.result {
            case .failure(let error):
                print(error)
                ModalController.showNegativeCustomAlertWith(title: "Connection error", msg: "")
            case .success(let responseObject):
                print("response is success:  \(responseObject)")
                let tempDict = responseObject as! NSDictionary
                let statusCheck = tempDict.object(forKey: "error") as! Bool
                if statusCheck == false{
                    self.employeeInfoDict = (tempDict).RemoveNullValueFromDic()
                    if let name = self.employeeInfoDict.object(forKey: "name") as? String {
                        self.signupFullName = name
                    }
                    if let emailID = self.employeeInfoDict.object(forKey: "email_id") as? String {
                        self.signupEmail = emailID
                    }
                    self.SignUpTable.reloadData()
                }else{
                    ModalController.showNegativeCustomAlertWith(title: "\(tempDict.object(forKey: "error_description") as! String)", msg: "")
                }
            }
        }
    }
    // MARK: - SignUpAPI
    func signupAPI(){
        if signupEmployeeCode!.isEmpty || signupFullName!.isEmpty || signupPassword!.isEmpty || signupConfirmPassword!.isEmpty
        {
            ModalController.showNegativeCustomAlertWith(title: "Please enter all fields", msg: "")
            return
        }
        if self.signupPassword! != self.signupConfirmPassword! {
            ModalController.showNegativeCustomAlertWith(title: "Password and confirm password doesn't match.", msg: "")
            return
        }
        ModalClass.startLoading(self.view)
        var device_id = ""
        let deviceIDStr = ModalController.getTheContentForKey("deviceID")
        if !(deviceIDStr is NSNull)
        {
            device_id = "\(ModalController.getTheContentForKey("deviceID")!)"
        }
        var device_Token = ""
        let deviceTokenStr = (ModalController.getTheContentForKey("firebaseToken") as AnyObject)
        if !(deviceTokenStr is NSNull)
        {
            device_Token = "\(ModalController.getTheContentForKey("firebaseToken") as AnyObject)"
        }
        let params = ["employee_id":"\(signupEmployeeCode!)",
            "name":"\(signupFullName!)",
            "pswd":"\(signupPassword!)",
            "device_id":device_id,
            "device_token":device_Token,
            "mob_no":"\(signupMobileNumber!)",
            "email_id":"\(signupEmail!)"]
        print(params)
        Alamofire.request("\(Constant.BASE_URL)Emp_Register.set", method:.post, parameters: params, encoding: URLEncoding.default).validate().responseJSON {
            response in
            ModalClass.stopLoading()
            
            switch response.result {
            case .failure(let error):
                print(error)
                ModalController.showNegativeCustomAlertWith(title: "Connection error", msg: "")
            case .success(let responseObject):
                print("response is success:  \(responseObject)")
                let tempDict = responseObject as! NSDictionary
                 ModalController.showSuccessCustomAlertWith(title: "\(tempDict.object(forKey: "error_description") as! String)", msg: "")
                
                self.signupEmployeeCode = ""
                self.signupFullName = ""
                self.signupPassword = ""
                self.signupConfirmPassword = ""
                self.SignUpTable.reloadData()
//                var isLoginThere = false
                
//                if var viewControllers = self.navigationController?.viewControllers
//                {
//                    for controller in viewControllers
//                    {
//                        if controller is LoginViewController
//                        {
//                            isLoginThere = true
//                            for controller in self.navigationController!.viewControllers as Array {
//                                if controller.isKind(of: LoginViewController.self) {
//                                    self.navigationController!.popToViewController(controller, animated: true)
//                                    break
//                                }
//                            }
//                        }
//                    }
//                }
//                if isLoginThere == false
//                {
//                    let vc = LoginViewController(nibName: "LoginViewController", bundle: nil)
//                    self.navigationController?.pushViewController(vc, animated: true)
//                }
//                let statusCheck = tempDict.object(forKey: "error") as! Bool
//                var errorCode = ""
//                if let result = tempDict.object(forKey: "error_code") as? String {
//                    errorCode = result
//                }
//                if let result =  tempDict.object(forKey: "error_code") as? NSNumber{
//                    errorCode = String(describing: result)
//                }
//                print("errorCode", errorCode)
//                if errorCode == "100" {
//                    let userDetails = tempDict.RemoveNullValueFromDic()
//                    ModalController.saveTheContent(userDetails, WithKey: "savedUserData")
//
//                }else{
//                    ModalController.showNegativeCustomAlertWith(title: "\(tempDict.object(forKey: "error_description") as! String)", msg: "")
//                }
            }
        }
    }
    
}
