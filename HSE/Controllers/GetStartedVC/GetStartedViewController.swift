//
//  GetStartedViewController.swift
//  HSE
//
//  Created by Neeraj Tiwari on 10/05/19.
//  Copyright © 2019 Sendan. All rights reserved.
//

import UIKit

class GetStartedViewController: UIViewController {
    @IBOutlet weak var bottomViewHeightConstant: NSLayoutConstraint!
    @IBOutlet weak var backBtnHeightConstant: NSLayoutConstraint!
    @IBOutlet weak var backbtn: UIButton!
    @IBOutlet weak var blurImageView: UIImageView!
    @IBOutlet weak var logoImageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if #available(iOS 11.0, *) {
            let window = UIApplication.shared.keyWindow
            let topPadding = window?.safeAreaInsets.top
            let myIntValue = Int(topPadding!)
            
            if myIntValue > 0{
                self.bottomViewHeightConstant.constant = 94
                self.backBtnHeightConstant.constant = 94
            }
        }
        if #available(iOS 12.0, *) {
            let window = UIApplication.shared.keyWindow
            let topPadding = window?.safeAreaInsets.top
            let myIntValue = Int(topPadding!)
            
            if myIntValue > 0{
                if myIntValue == 44{
                    self.bottomViewHeightConstant.constant = 94
                    self.backBtnHeightConstant.constant = 94
                }else{
                    self.bottomViewHeightConstant.constant = 70
                    self.backBtnHeightConstant.constant = 94
                }
            }
        }
      self.blurImageView.alpha = 1

        //Looks for single or multiple taps.
      

    }
    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    @IBAction func backbtnAction(_ sender: Any) {
        let vc = SelectOptionViewController(nibName: "SelectOptionViewController", bundle: nil)
        self.navigationController?.pushViewController(vc, animated: true)
//        let vc = HomeViewController(nibName: "HomeViewController", bundle: nil)
//        self.navigationController?.pushViewController(vc, animated: true)
    }
}
