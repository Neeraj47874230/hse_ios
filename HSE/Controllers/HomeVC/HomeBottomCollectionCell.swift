//
//  HomeBottomCollectionCell.swift
//  HSE
//
//  Created by Neeraj tiwari on 19/05/19.
//  Copyright © 2019 Sendan. All rights reserved.
//

import UIKit

class HomeBottomCollectionCell: UICollectionViewCell {
    @IBOutlet weak var sideLine: UIView!
    
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var txtLbl: UILabel!
    @IBOutlet weak var bottomLine: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
