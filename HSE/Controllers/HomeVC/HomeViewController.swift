//
//  HomeViewController.swift
//  HSE
//
//  Created by Neeraj tiwari on 19/05/19.
//  Copyright © 2019 Sendan. All rights reserved.
//

import UIKit
import SideMenu

class HomeViewController: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate {
    
    @IBOutlet weak var collectionView: UICollectionView!
    var textArray : [String] = [String]()
    var imageArray : [String] = [String]()
    @IBOutlet weak var topViewHeightConstant: NSLayoutConstraint!
    var userDetailDict : NSDictionary = NSDictionary()

    override func viewDidLoad() {
        super.viewDidLoad()
        initialiseVariables()
        registerCollectionCells()
        sideMenuCode()
        self.setupUiMethod()
        let   userInfo = ModalController.getTheContentForKey("savedUserData") as! NSDictionary
        userDetailDict = userInfo.RemoveNullValueFromDic()
       
        // Do any additional setup after loading the view.
    }
    func setupUiMethod(){
       
        if #available(iOS 11.0, *) {
            let window = UIApplication.shared.keyWindow
            let topPadding = window?.safeAreaInsets.top
            let myIntValue = Int(topPadding!)
            
            if myIntValue > 0{
                self.topViewHeightConstant.constant = 92
            }
        }
        if #available(iOS 12.0, *) {
            let window = UIApplication.shared.keyWindow
            let topPadding = window?.safeAreaInsets.top
            let myIntValue = Int(topPadding!)
            
            if myIntValue > 0{
                if myIntValue == 44{
                    self.topViewHeightConstant.constant = 92
                }else{
                    self.topViewHeightConstant.constant = 68
                }
            }
        }
        self.navigationController?.isNavigationBarHidden = true
       
    }
    
    // MARK:- UserDefinedFunctions
    func registerCollectionCells()
    {
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        self.collectionView.collectionViewLayout = layout
        self.collectionView.register(UINib(nibName:"HomeTopCollectionCell", bundle: nil), forCellWithReuseIdentifier: "HomeTopCollectionCell")
        self.collectionView.register(UINib(nibName:"HomeBottomCollectionCell", bundle: nil), forCellWithReuseIdentifier: "HomeBottomCollectionCell")
    }
    func initialiseVariables()
    {
        textArray = ["00 \nSafety Observation","00 \nIncident Report","00 \nInspection Report","00 \nReports","00 \nAudit","00 \nOSHAS","00 \nNotification","00 \nReminder","00 \nAccount","00 \nAbout Us"]
         imageArray = ["home_safety_observation.png","home_incidentReport.png","home-inspection_report.png","home_report.png","home_audit.png","home_Oshas.png","home-notification.png","home_reminder.png","home_account.png","home_aboutUs.png"]
        
        
    }
    
    func sideMenuCode()
    {
        let vc = SideMenuHomeViewController(nibName: "SideMenuHomeViewController", bundle: nil)
        
        let menuLeftNavigationController =  UISideMenuNavigationController(rootViewController: vc)
       
        SideMenuManager.default.menuLeftNavigationController = menuLeftNavigationController
 
        SideMenuManager.default.menuAddPanGestureToPresent(toView: self.navigationController!.navigationBar)
        SideMenuManager.default.menuAddScreenEdgePanGesturesToPresent(toView: self.navigationController!.view)
        SideMenuManager.default.menuPresentMode = .menuDissolveIn
        // (Optional) Prevent status bar area from turning black when menu appears:
        SideMenuManager.default.menuFadeStatusBar = false
    }
    
    @IBAction func sideMenuAction(_ sender: Any) {
        present(SideMenuManager.default.menuLeftNavigationController!, animated: true, completion: nil)

    }
    
    // MARK:- UICollectionViewMethods
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
        
    {
        if (section == 0){
            return 1;
        }
        return textArray.count
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        if (indexPath.section == 0){
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeTopCollectionCell", for: indexPath) as! HomeTopCollectionCell
            print(userDetailDict)

            cell.nameLbl.text = userDetailDict.object(forKey: "emp_name") as? String
             cell.designationLbl.text = userDetailDict.object(forKey: "JOB") as? String
            
            return cell;
        }
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeBottomCollectionCell", for: indexPath) as! HomeBottomCollectionCell
        cell.txtLbl.text = textArray[indexPath.item]
        cell.imgView.image = UIImage.init(named: imageArray[indexPath.item])

        cell.sideLine.isHidden = true
        if (indexPath.item % 2 == 0)
        {
            cell.sideLine.isHidden = false
        }
        
        
        return cell;
    }
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int
    {
        return 2
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.section == 0
        {
//            let vc = DetailViewController(nibName: "DetailViewController", bundle: nil)
//            self.navigationController?.pushViewController(vc, animated: true)
            return
        }
        let vc = UserProfileViewController(nibName: "UserProfileViewController", bundle: nil)
        self.navigationController?.pushViewController(vc, animated: true)

    }
    
}
extension HomeViewController : UICollectionViewDelegateFlowLayout {
    //1
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        if (indexPath.section == 0){
            
            return CGSize(width:UIScreen.main.bounds.size.width , height: (UIScreen.main.bounds.size.height)/4)
        }
        return CGSize(width:(UIScreen.main.bounds.size.width - 40)/2 , height: 120)
        
    }
    
    //3
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        if (section == 0){
            
            return UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: 0)
        }
        return UIEdgeInsets.init(top: 15, left: 20, bottom: 0, right: 20)
        
    }
    
    // 4
    
}
