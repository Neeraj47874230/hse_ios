//
//  SearchViewController.swift
//  HSE
//
//  Created by Neeraj tiwari on 19/05/19.
//  Copyright © 2019 Sendan. All rights reserved.
//

import UIKit


protocol SearchViewDelegate {
    func searchResultSelected(tag : NSInteger, str : String)
}

class SearchViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate {
    var textArray : [String] = [String]()
    var tempArr : [String] = [String]()
    var delegate : SearchViewDelegate?
    var selectedTag : NSInteger = 0
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var txtFld: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tblView.estimatedRowHeight = 45;
        initialiseVariables()
        registerTableCells()
        // Do any additional setup after loading the view.
    }

    // MARK: - UserDefinedFunctions
    
    @IBAction func backBtnAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    func registerTableCells()
    {
        self.tblView.register(UINib(nibName:"SearchCell",bundle: nil),forCellReuseIdentifier: "SearchCell")
       
        
    }
    func initialiseVariables()
    {
        textArray = ["Close Call","Unsafe Act","Unsafe Condition","Positive Observation","LSR"]
        tempArr = ["Close Call","Unsafe Act","Unsafe Condition","Positive Observation","LSR"]
        
        let indentView1 = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 40))
        txtFld.leftView = indentView1
        txtFld.leftViewMode = .always

    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    // MARK: - UITextFieldDelegate
     func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool // return NO to not change text
     {
        if let text = textField.text,
            let textRange = Range(range, in: text) {
            let updatedText = text.replacingCharacters(in: textRange,
                                                       with: string)
            searchRecordsAsPerText(updatedText)
        }
        return true
    }
    func searchRecordsAsPerText(_ str:String)
    {
        textArray = tempArr
        if ( str.count > 0)
        {
            var predicate: NSPredicate?
            predicate = NSPredicate(format: "SELF contains[cd] %@", str)
            
            
            textArray = textArray.filter { predicate!.evaluate(with: $0) }
        
        }
        self.tblView.reloadData()
        
        
    }
    
   
    
     func textFieldShouldReturn(_ textField: UITextField) -> Bool // called when 'return' key pressed. return NO to ignore.
     {
        textField.resignFirstResponder()
        return true;
    }
    
    // MARK: - UITableViewMethods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
      
        
        return textArray.count;
    }
    
    
    // Row display. Implementers should *always* try to reuse cells by setting each cell's reuseIdentifier and querying for available reusable cells with dequeueReusableCellWithIdentifier:
    // Cell gets various attributes set automatically based on table (separators) and data source (accessory views, editing controls)
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SearchCell", for: indexPath) as! SearchCell
        cell.txtLbl.text = textArray[indexPath.row]
        
        return cell;
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        self.delegate?.searchResultSelected(tag: selectedTag, str: textArray[indexPath.row])
        self.navigationController?.popViewController(animated: true)
    }

    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return UITableView.automaticDimension
    }

}
