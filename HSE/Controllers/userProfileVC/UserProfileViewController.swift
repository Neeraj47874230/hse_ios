//
//  UserProfileViewController.swift
//  HSE
//
//  Created by Neeraj Tiwari on 19/05/19.
//  Copyright © 2019 Sendan. All rights reserved.
//

import UIKit
import Alamofire
class UserProfileViewController: UIViewController {
   
    @IBOutlet weak var notificationBtn: UIButton!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var designationLabel: UILabel!
    @IBOutlet weak var helpbtn: UIButton!
    @IBOutlet weak var createBtn: UIButton!
    @IBOutlet weak var viewBtn: UIButton!
    @IBOutlet weak var actionBtn: UIButton!
    @IBOutlet weak var getDataBtn: UIButton!
    var userDetailDict : NSDictionary = NSDictionary()

    @IBOutlet weak var menuBtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        let   userInfo = ModalController.getTheContentForKey("savedUserData") as! NSDictionary
        userDetailDict = userInfo.RemoveNullValueFromDic()
        self.nameLabel.text = userDetailDict.object(forKey: "emp_name") as? String
        self.designationLabel.text = userDetailDict.object(forKey: "JOB") as? String
        
    }
    @IBAction func backBtnClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func notificationBtnClicked(_ sender: Any) {
        let vc = DetailViewController(nibName: "DetailViewController", bundle: nil)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func helpBtn(_ sender: Any) {
    }
    @IBAction func createBtnClicked(_ sender: Any) {
        self.checkAccess(typeStr: "1")
    }
    @IBAction func viewBtnClicked(_ sender: Any) {
        self.checkAccess(typeStr: "2")
    }
    @IBAction func actionBtnClicked(_ sender: Any) {
         self.checkAccess(typeStr: "3")
    }
    @IBAction func getDataClicked(_ sender: Any) {
        self.checkAccess(typeStr: "4")
    }
    func checkAccess(typeStr: String) {
        ModalClass.startLoading(self.view)
        var device_id = ""
        let deviceIDStr = ModalController.getTheContentForKey("deviceID")
        if !(deviceIDStr is NSNull)
        {
            device_id = "\(ModalController.getTheContentForKey("deviceID")!)"
        }
        var device_Token = ""
        let deviceTokenStr = (ModalController.getTheContentForKey("firebaseToken") as AnyObject)
        if !(deviceTokenStr is NSNull)
        {
            device_Token = "\(ModalController.getTheContentForKey("firebaseToken") as AnyObject)"
        }
        let userDict = ModalController.getTheContentForKey("savedUserData") as! NSDictionary
        var employeeID = ""
        if let emp = userDict.object(forKey: "emp_id") as? String {
            employeeID = emp
        }
        if let empNumber = userDict.object(forKey: "emp_id") as? NSNumber {
            employeeID = "\(empNumber)"
        }
        var password = ""
        if let psswrd = userDict.object(forKey: "PASSWORD") as? String {
            password = psswrd
        }
        if let psswrdNumber = userDict.object(forKey: "PASSWORD") as? NSNumber {
            password = "\(psswrdNumber)"
        }
        let params = ["employee_id":employeeID,
            "pswd": password,
            "device_id":device_id,
            "device_token":device_Token,
            "DEVICE_TYPE":"IOS",
            "check_access":"1"]
        
        print(params)
        Alamofire.request("\(Constant.BASE_URL)Userlogin.set", method:.post, parameters: params, encoding: URLEncoding.default).validate().responseJSON {
            response in
            ModalClass.stopLoading()
            
            switch response.result {
            case .failure(let error):
                print(error)
                ModalController.showNegativeCustomAlertWith(title: "Connection error", msg: "")
            case .success(let responseObject):
                print("response is success:  \(responseObject)")
                let tempDict = responseObject as! NSDictionary
                let statusCheck = tempDict.object(forKey: "error_description") as! String
                
                var sorUserType = ""
                if let userType = userDict.object(forKey: "SOR_USER_TYPE") as? String {
                    sorUserType = userType
                }
                if let userTypeNumber = userDict.object(forKey: "PASSWORD") as? NSNumber {
                    sorUserType = "\(userTypeNumber)"
                }
                let errorType = tempDict.object(forKey: "error") as! Bool
                
                if errorType == false {
                    
                    if sorUserType == "3" {
                        ModalController.showNegativeCustomAlertWith(title: "Info", msg: "You are not eligable for this option.")
                    }else {
                        if typeStr == "1" {
                            let vc = SORViewController(nibName: "SORViewController", bundle: nil)
                            self.navigationController?.pushViewController(vc, animated: true)
                        }else if typeStr == "2"{
                            let vc = DetailViewController(nibName: "DetailViewController", bundle: nil)
                            self.navigationController?.pushViewController(vc, animated: true)
                        }else if typeStr == "3" {
                            let vc = DetailViewController(nibName: "DetailViewController", bundle: nil)
                            self.navigationController?.pushViewController(vc, animated: true)
                        }else if typeStr == "4" {
                            
                        }
                    }
                }else{
                    ModalController.showNegativeCustomAlertWith(title: "\(tempDict.object(forKey: "error_description") as! String)", msg: "")
                }
            }
        }
    }
    
}
