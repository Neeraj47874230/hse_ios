//
//  LoginViewController.swift
//  HSE
//
//  Created by Neeraj Tiwari on 18/05/19.
//  Copyright © 2019 Sendan. All rights reserved.
//

import UIKit
import Alamofire
class LoginViewController: UIViewController {
    @IBOutlet weak var signUpBtn: UIButton!
    @IBOutlet weak var signUpLowerLabel: UILabel!
    @IBOutlet weak var loginBtn: UIButton!
    @IBOutlet weak var employeeCodeTxtFld: UITextField!
    @IBOutlet weak var passwordTxtFld: UITextField!
    @IBOutlet weak var hideShowPasswordBtn: UIButton!
   
    @IBOutlet weak var countinueBtn: UIButton!
    
   
    @IBOutlet weak var loginLowerLabel: UILabel!
   
    override func viewDidLoad() {
        super.viewDidLoad()
        self.loginLowerLabel.backgroundColor = UIColor(red: 206.0/255.0, green: 120.0/255.0, blue: 81.0/255.0, alpha: 1.0) ;
        self.signUpLowerLabel.isHidden = true ;
        self.loginBtn.isSelected = true
        self.signUpBtn.isSelected = false

        self.hideShowPasswordBtn.isSelected = false;
        self.passwordTxtFld.isSecureTextEntry = true;

        self.employeeCodeTxtFld.layer.cornerRadius = 12.0
        self.employeeCodeTxtFld.layer.masksToBounds = true
        self.employeeCodeTxtFld.attributedPlaceholder = NSAttributedString(string: "Employee Code/Email", attributes: [NSAttributedString.Key.foregroundColor: UIColor(red: 0.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 1.0)])
        let indentView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 20))
        self.employeeCodeTxtFld.leftView = indentView
        self.employeeCodeTxtFld.leftViewMode = .always
        
        self.passwordTxtFld.layer.cornerRadius = 12.0
        self.passwordTxtFld.layer.masksToBounds = true
        self.passwordTxtFld.attributedPlaceholder = NSAttributedString(string: "Password", attributes: [NSAttributedString.Key.foregroundColor: UIColor(red: 0.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 1.0)])
        let indentView1 = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 20))
        self.passwordTxtFld.leftView = indentView1
        self.passwordTxtFld.leftViewMode = .always
        
        self.countinueBtn.layer.cornerRadius = 12.0
        self.countinueBtn.layer.masksToBounds = true
        
    }

    override func viewWillAppear(_ animated: Bool) {
        self.loginLowerLabel.backgroundColor = UIColor(red: 206.0/255.0, green: 120.0/255.0, blue: 81.0/255.0, alpha: 1.0) ;
        self.signUpLowerLabel.isHidden = true ;
        self.loginLowerLabel.isHidden = false ;
        self.signUpBtn.isSelected = false
        self.loginBtn.isSelected = true
    }
    @IBAction func signUpBtnClicked(_ sender: Any) {
        self.signUpLowerLabel.backgroundColor =  UIColor(red: 206.0/255.0, green: 120.0/255.0, blue: 81.0/255.0, alpha: 1.0) ;
        self.loginLowerLabel.isHidden = true
        self.signUpLowerLabel.isHidden = false ;
        self.signUpBtn.isSelected = true
        self.loginBtn.isSelected = false
        
         var isSignUp = false
        for item in self.navigationController?.viewControllers as! [UIViewController] {
            if item is SignUPViewController
            {
                isSignUp = true
                let signUp = item as! SignUPViewController
                self.navigationController?.popToViewController(signUp, animated: true)
                
                break;
            }
        }
        if isSignUp == false {
            let vc = SignUPViewController(nibName: "SignUPViewController", bundle: nil)
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    @IBAction func loginBtnClicked(_ sender: Any) {
        self.loginLowerLabel.backgroundColor = UIColor(red: 206.0/255.0, green: 120.0/255.0, blue: 81.0/255.0, alpha: 1.0) ;
        self.signUpLowerLabel.isHidden = true ;
        self.loginLowerLabel.isHidden = false ;
        self.signUpBtn.isSelected = false
        self.loginBtn.isSelected = true
    }
    @IBAction func hideShowPasswordBtnClicked(_ sender: Any) {
        self.passwordTxtFld.isSecureTextEntry = true;
        if (hideShowPasswordBtn.isSelected ) {
            self.hideShowPasswordBtn.isSelected = false;
            self.passwordTxtFld.isSecureTextEntry = true;
        } else {
            self.hideShowPasswordBtn.isSelected = true;
            self.passwordTxtFld.isSecureTextEntry = false;
        }
    }
    @IBAction func countinueBtnClicked(_ sender: Any) {
        if self.employeeCodeTxtFld.text!.isEmpty && self.passwordTxtFld.text!.isEmpty {
            ModalController.showNegativeCustomAlertWith(title: "Please enter both fields.", msg: "")
        }
        else if self.employeeCodeTxtFld.text!.isEmpty {
            ModalController.showNegativeCustomAlertWith(title: "Please enter employee code.", msg: "")
        }
        else if self.passwordTxtFld.text!.isEmpty {
            ModalController.showNegativeCustomAlertWith(title: "Please enter password.", msg: "")
        }else {
            loginAPI()
        }
    }
    // MARK: - loginAPI
    func loginAPI(){
        ModalClass.startLoading(self.view)
        var device_id = ""
        let deviceIDStr = ModalController.getTheContentForKey("deviceID")
        if !(deviceIDStr is NSNull)
        {
            device_id = "\(ModalController.getTheContentForKey("deviceID")!)"
        }
        var device_Token = ""
        let deviceTokenStr = (ModalController.getTheContentForKey("firebaseToken") as AnyObject)
        if !(deviceTokenStr is NSNull)
        {
            device_Token = "\(ModalController.getTheContentForKey("firebaseToken") as AnyObject)"
        }
        let params = ["employee_id":"\(self.employeeCodeTxtFld.text!)",
            "pswd":"\(self.passwordTxtFld.text!)",
            "device_id":device_id,
            "device_token":device_Token,
            "DEVICE_TYPE":"IOS"]
        print(params)
       
        Alamofire.request("\(Constant.BASE_URL)Userlogin.set", method:.post, parameters: params, encoding: URLEncoding.default).validate().responseJSON {
            response in
            ModalClass.stopLoading()
            
            switch response.result {
            case .failure(let error):
                print(error)
                ModalController.showNegativeCustomAlertWith(title: "Connection error", msg: "")
            case .success(let responseObject):
                print("response is success:  \(responseObject)")
                let tempDict = responseObject as! NSDictionary
                let statusCheck = tempDict.object(forKey: "error") as! Bool
                if statusCheck == false {
                    let userDetails = tempDict.RemoveNullValueFromDic()
                    ModalController.saveTheContent(userDetails, WithKey: "savedUserData")
                    
                    ModalController.showSuccessCustomAlertWith(title: "Login Successful.", msg: "")
                
                    
                    let vc = HomeViewController(nibName: "HomeViewController", bundle: nil)
                    self.navigationController?.pushViewController(vc, animated: true)
                }else{
                    ModalController.showNegativeCustomAlertWith(title: "\(tempDict.object(forKey: "error_description") as! String)", msg: "")
                }
            }
        }
    }
}
