//
//  ModalClass.swift
//  Silent Panda
//
//  Created by Neeraj Tiwari on 10/5/17.
//  Copyright © 2017 aishwarya sanganan. All rights reserved.
//

import UIKit

@available(iOS 10.0, *)
class ModalController: NSObject {
    
    
    static func showAlertWith(message: String, title: String )
    {
         UIAlertView.init(title: title, message: message , delegate: nil, cancelButtonTitle: "OK").show();
    }
    
    static func showSuccessCustomAlertWith(title: String, msg: String)
    {
        let banner = Banner(title: title, subtitle: msg, image: UIImage(named: "like_thumb"), backgroundColor: UIColor(red:48.00/255.0, green:174.0/255.0, blue:51.0/255.0, alpha:1.000))
        banner.dismissesOnTap = true
        banner.show(duration: 2.5)
    }
    
    static func showNegativeCustomAlertWith(title: String, msg: String)
    {
        let banner = Banner(title: title, subtitle: msg, image: UIImage(named: ""), backgroundColor: UIColor(red: 186.0/255, green: 30.0/255, blue: 40.0/255, alpha: 1.0))
        banner.dismissesOnTap = true
        banner.show(duration: 1.5)
    }
    
    // MARK: Appdelegate Object
    static func APPDELEGATE() -> AppDelegate{
        return UIApplication.shared.delegate as! AppDelegate;
    }
   static func isValidEmail(testStr:String) -> Bool {
        // print("validate calendar: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    //MARK: NSUserDefaultMethods
    static func saveTheContent(_ content : AnyObject?, WithKey key : String )-> Void {
        let defaults = UserDefaults.standard
        defaults.set(content, forKey: key as String)
        defaults.synchronize();
    }
    ///Users/aishwaryasanganan/Documents/projects/SceneAhead_ios/Scene Ahead/Scene Ahead/Scene Ahead.entitlements
    
    static func removeTheContentForKey(_ key : NSString )-> Void {
        let defaults = UserDefaults.standard
        defaults.removeObject(forKey: key as String)
    }
    static func getTheContentForKey(_ key : NSString)-> AnyObject? {
        let defaults = UserDefaults.standard
        let value = defaults.object(forKey: key as String)
        return value as AnyObject
    }
    static func checkStringIsValid(_ str : NSString)-> Bool {
        if (str .isKind(of: NSNull .classForCoder()) || str .trimmingCharacters(in: CharacterSet.whitespaces).isEmpty){
            return false
        }
        return true
    }
    static func checkOriginlStringIsValid(_ str : AnyObject? )-> Bool {

        if str == nil{
            return false
        }else if str is NSNull
        {
            return false
        }else if str is String
        {
            let finalStr : String = str as! String
            if finalStr.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty == true
            {
                return false
            }
        }
        return true
    }
    static func openMaps(latitute : AnyObject? , longitute : AnyObject? ) {
        
       
    }
    static  func base64Encoded(_ encodeString:String) -> String
    {
        let plainData =  (encodeString as NSString).data(using: String.Encoding.utf8.rawValue)
        let base64String = plainData?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        print("base64String\n",base64String!);
        return base64String!
    }
    static   func base64Decoded(_ decoString:String) -> String
    {
        let decodedData = Data(base64Encoded: decoString, options:NSData.Base64DecodingOptions(rawValue: 0))
        if let decode = decodedData{
            let decodedString = NSString(data: decode, encoding: String.Encoding.utf8.rawValue)
            
            if decodedString == nil {
                return decoString;
            }
            print("decode string \n",decodedString! );
            return decodedString as! String;
        }
        else
        {
            return decoString;
        }
        
    }
  static  func removeSpecialCharsFromString(text: String) -> String {
        let okayChars = Set("abcdefghijklmnopqrstuvwxyz ABCDEFGHIJKLKMNOPQRSTUVWXYZ")
        return text.filter {okayChars.contains($0) }
    }
    static  func setGradientBackgroundFromModal(view : UIView)-> UIView {
        let colorTop =  UIColor(red: 5.0/255.0, green: 10.0/255.0, blue: 15.0/255.0, alpha: 1.0).cgColor
        let colorBottom = UIColor(red: 67.0/255.0, green: 82.0/255.0, blue: 102.0/255.0, alpha: 1.0).cgColor
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [colorTop, colorBottom]
        gradientLayer.locations = [0.0, 1.0]
        let screenSize = UIScreen.main.bounds
        let screenWidth = screenSize.width
        let screenHeight = screenSize.height
        gradientLayer.frame =  CGRect(x: 0 , y: 0, width: screenWidth, height: screenHeight)
        view.layer.insertSublayer(gradientLayer, at: 0)
        return view
    }
    
static func isStringValid(_ str: String?) -> Bool {
    if (str?.trimmingCharacters(in: CharacterSet.whitespaces).count ?? 0) == 0 || (str is NSNull) || str == nil || (str?.replacingOccurrences(of: "\n", with: "").count ?? 0) == 0 {
            return false
        }
        return true
    }
    

}
