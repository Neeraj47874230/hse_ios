//
//  Constant.swift
//  BaseProjectSwift
//                          
//  Created by Sufalam5 on 12/3/15.
//  Copyright © 2015 Sufalam Technologies. All rights reserved.
//
import UIKit

class Constant: NSObject {

    // domian URL
    static let BASE_URL : String = "http://db.sendan.com.sa/api/v1/sor/";
    
//AISHWARYA
    static let authenticationID : String = "authenticationID";
    static let authenticationKEY : String = "authenticationKEY";
    static let savedUserData : String = "savedUserData";
    static let rememberEmployeeCode : String = "rememberEmployeeCode";
    static let rememberPassword : String = "rememberPassword";
    static let deviceID : String = "deviceID";
    let appBase_URL = "appBaseURL"

    
    
    static let deviceType : String = "IOS";
    static let userToken : String = "userToken";
    static let userLatitude : String = "userLatitude";
    static let userLongitude : String = "userLongitude";
    static let saveUsername : String = "saveUsername";
    static let savePassword : String = "savePassword";
    static let firebaseToken : String = "firebaseToken";
    static let userTokenForPending : String = "userTokenForPending";
    static let s3BaseURL : String = "http://s3.us-east-2.amazonaws.com"
    
    
    static let VIMEO_API_URL:String = "https://api.vimeo.com"
    static let VIMEO_ACCESS_TOKEN:String = "c4ab6224e971689e0a79cde46df71eb1"
    
    static let FILE_UPLOAD_URL : String = "/restapi/uploadimageforparticipant.json";
    static let CRASHLYTICS_KEY : String = "d14483a4f8fa3dc184cec23668b9c0ea6c3b4d90";
    static let DATE_FORMAT : String = "yyyy-MM-dd"
//    NSString * const GOOGLEANALYTICS_KEY = @"UA-27089046-5";
    //Added by Jignesh to Test
    static let GOOGLEANALYTICS_KEY : String = "UA-58783058-1";
    static let GACCESSTOKEN : String = "gotubeAccesstoekn";
    static let GSAVEDACCESTOKEN : String = "GSAVEDACCESTOKEN";
    static let GACCESSTOKENAPPHAVE : String = "GACCESSTOKENAPPHAVE";
    static let refreshToken : String = "refreshToken";
    static let userEmail : String = "userEmail";
    static let bucketName:String = "sceneahead";
    
    static var localCircleId : String = "";
    static var globalCircleId : String = "";
    
    static let SUCCESS : String = "Success";
    static let FAILURE : String = "Failure";
    static let ME : String = "- Me";
    static let SCHOOL :String = "School";
    static let LOCAL :String = "Local";
    static let GLOBAL :String = "Global";
    static let MYPOST :String = "My Posts"
    static let REPORT :String = "report"
    static let UNHIDE_TABLE :String = "UNHIDETABLE"
    static let SEARCH :String = "Search"
    static let AUTHTYPE: Int = 3;
    static var startDate :String = ""

    //1 = None, 2 for Basic, 3 for Digest, 4 for OAuth
   // static let USERNAME: String = "ra34vi";
  //  static let PASSWORD: String = "d460ccbdea1789adb0b8b057e68c721c585ff87b";
    static let BACKGROUNDCELL:UIColor =  UIColor(red: 56/255.0, green: 56/255.0, blue: 56/255.0, alpha: 1.0)
    static let PLACEHOLDERCOLOR:UIColor = UIColor(red: 149/255.0, green: 179/255.0, blue: 215/255.0, alpha: 1.0)
    
     static let FONT_System_Medium : NSString = "Medium";
    
    //MARK: - set Fonts
    static let FONT_Calibri : NSString = "Calibri";
    static let FONT_CalibriBold : NSString = "Calibri-Bold"
    static let FONT_CalibriBoldItalic : NSString = "Calibri-BoldItalic"
    static let FONT_CalibrItalic : NSString = "Calibri-Italic"
    
}
