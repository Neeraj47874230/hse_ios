//
//  WebModel.swift
//  ApiDemo
//
//  Created by gaurav on 11/30/17.
//  Copyright © 2017 gaurav. All rights reserved.
//

import UIKit
import Alamofire


class WebModel: NSObject {
    
    
    static func makePostRequest(_ urlString: String, withParameters: String , completion: ((AnyObject?,Bool) -> Void)?)
    {
        let todoEndpoint: String = urlString
        let post :  String = withParameters
        var request = URLRequest(url: URL(string: todoEndpoint)!)
        request.httpBody = post.data(using: String.Encoding.ascii, allowLossyConversion: true)
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.httpMethod = "POST"
        
        Alamofire.request(request as URLRequestConvertible).responseString { response in
            guard response.result.error == nil else {
                // got an error in getting the data, need to handle it
                
                completion!(nil,false)
                
                print("error calling GET on /todos/1")
                print(response.result.error!)
                return
            }
            
            // make sure we got some JSON since that's what we expect
            guard let json = response.result.value as? [String: Any] else {
                completion!(response as AnyObject,false)
                
                print("didn't get todo object as JSON from API")
                print("Error: \(String(describing: response.result.error))")
                return
            }
            
            completion!(json as AnyObject,true)
            
        }
        
        
    }
    // make post request with formData in body
    static func makePostRequestwithFormData(_ urlString: String, withParameters: String , completion: ((AnyObject?,Bool) -> Void)?)
    {
        let todoEndpoint: String = urlString
        let post :  String = withParameters
        var request = URLRequest(url: URL(string: todoEndpoint)!)
        request.httpBody = post.data(using: String.Encoding.ascii, allowLossyConversion: true)
        request.setValue("application/form-data", forHTTPHeaderField: "Content-Type")
        request.httpMethod = "POST"
        
        Alamofire.request(request as URLRequestConvertible).responseString { response in
            guard response.result.error == nil else {
                // got an error in getting the data, need to handle it
                
                completion!(nil,false)
                
                print("error calling GET on /todos/1")
                print(response.result.error!)
                return
            }
            
            // make sure we got some JSON since that's what we expect
            guard let json = response.result.value as? [String: Any] else {
                completion!(response as AnyObject,false)
                
                print("didn't get todo object as JSON from API")
                print("Error: \(String(describing: response.result.error))")
                return
            }
            
            completion!(json as AnyObject,true)
            
        }
        
        
    }
    static func makePostRequestWithToken(_ urlString: String, withParameters: String , completion: ((AnyObject?,Bool) -> Void)?)
    {
        let todoEndpoint: String = urlString
        let post :  String = withParameters
        var request = URLRequest(url: URL(string: todoEndpoint)!)
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("Bearer \(ModalController.getTheContentForKey("userToken") as! String)", forHTTPHeaderField: "Authorization")
        request.httpBody = post.data(using: String.Encoding.ascii, allowLossyConversion: true)
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.httpMethod = "POST"
        
        Alamofire.request(request as URLRequestConvertible).responseJSON { response in
            guard response.result.error == nil else {
                // got an error in getting the data, need to handle it
                
                completion!(nil,false)
                
                print("error calling GET on /todos/1")
                print(response.result.error!)
                return
            }
            
            // make sure we got some JSON since that's what we expect
            guard let json = response.result.value as? [String: Any] else {
                completion!(response as AnyObject,false)
                
                print("didn't get todo object as JSON from API")
                print("Error: \(String(describing: response.result.error))")
                return
            }
            
            completion!(json as AnyObject,true)
            
        }
        
        
    }
    static func makePostRequestWithTokenForPendingRequest(_ urlString: String, withParameters: String , completion: ((AnyObject?,Bool) -> Void)?)
    {
        let todoEndpoint: String = urlString
        let post :  String = withParameters
        var request = URLRequest(url: URL(string: todoEndpoint)!)
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("Bearer \(ModalController.getTheContentForKey("userTokenForPending") as! String)", forHTTPHeaderField: "Authorization")
        request.httpBody = post.data(using: String.Encoding.ascii, allowLossyConversion: true)
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.httpMethod = "POST"
        
        Alamofire.request(request as URLRequestConvertible).responseJSON { response in
            guard response.result.error == nil else {
                // got an error in getting the data, need to handle it
                
                completion!(nil,false)
                
                print("error calling GET on /todos/1")
                print(response.result.error!)
                return
            }
            
            // make sure we got some JSON since that's what we expect
            guard let json = response.result.value as? [String: Any] else {
                completion!(response as AnyObject,false)
                
                print("didn't get todo object as JSON from API")
                print("Error: \(String(describing: response.result.error))")
                return
            }
            
            completion!(json as AnyObject,true)
            
        }
        
        
    }
    static func makeGetRequest(_ urlString: String, completion: ((AnyObject?,Bool) -> Void)? )
    {
        let todoEndpoint: String = urlString
        Alamofire.request(todoEndpoint)
            .responseJSON { response in
                // check for errors
                guard response.result.error == nil else {
                    // got an error in getting the data, need to handle it
                    
                    completion!(nil,false)
                    
                    print("error calling GET on /todos/1")
                    print(response.result.error!)
                    return
                }
                
                // make sure we got some JSON since that's what we expect
                guard let json = response.result.value as? [String: Any] else {
                    completion!(response as AnyObject,false)

                    print("didn't get todo object as JSON from API")
                    print("Error: \(String(describing: response.result.error))")
                    return
                }
                
                completion!(json as AnyObject,true)
                
        }
    }
    static func makeGetRequestWithToken(_ urlString: String, completion: ((AnyObject?,Bool) -> Void)? )
    {
        let todoEndpoint: String = urlString
   //     let post :  String = withParameters
        var request = URLRequest(url: URL(string: todoEndpoint)!)
//        request.httpBody = post.data(using: String.Encoding.ascii, allowLossyConversion: true)
//        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("Bearer \(ModalController.getTheContentForKey("userToken") as! String)", forHTTPHeaderField: "Authorization")
        print("token",ModalController.getTheContentForKey("userToken") as! String)
        request.httpMethod = "GET"
        
        Alamofire.request(request as URLRequestConvertible).responseJSON{ response in
        
        
//        let todoEndpoint: String = urlString
//        Alamofire.request(todoEndpoint)
  //          .responseJSON { response in
                // check for errors
                guard response.result.error == nil else {
                    // got an error in getting the data, need to handle it
                    
                    completion!(nil,false)
                    
                    print("error calling GET on /todos/1")
                    print(response.result.error!)
                    return
                }
                
                // make sure we got some JSON since that's what we expect
                guard let json = response.result.value as? [String: Any] else {
                    completion!(response as AnyObject,false)
                    
                    print("didn't get todo object as JSON from API")
                    print("Error: \(String(describing: response.result.error))")
                    return
                }
                
                completion!(json as AnyObject,true)
                
        }
    }
    static func makeGetRequestWithTokenForPending(_ urlString: String, completion: ((AnyObject?,Bool) -> Void)? )
    {
        let todoEndpoint: String = urlString
        //     let post :  String = withParameters
        var request = URLRequest(url: URL(string: todoEndpoint)!)
        //        request.httpBody = post.data(using: String.Encoding.ascii, allowLossyConversion: true)
        //        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("Bearer \(ModalController.getTheContentForKey("userTokenForPending") as! String)", forHTTPHeaderField: "Authorization")
        request.httpMethod = "GET"
        
        Alamofire.request(request as URLRequestConvertible).responseJSON{ response in
            //        let todoEndpoint: String = urlString
            //        Alamofire.request(todoEndpoint)
            //          .responseJSON { response in
            // check for errors
            guard response.result.error == nil else {
                // got an error in getting the data, need to handle it
                
                completion!(nil,false)
                
                print("error calling GET on /todos/1")
                print(response.result.error!)
                return
            }
            
            // make sure we got some JSON since that's what we expect
            guard let json = response.result.value as? [String: Any] else {
                completion!(response as AnyObject,false)
                
                print("didn't get todo object as JSON from API")
                print("Error: \(String(describing: response.result.error))")
                return
            }
            
            completion!(json as AnyObject,true)
            
        }
    }
    // MARK: WebService use
    static func stringToDictionary(_ strToJSON : String)-> NSDictionary!{
        print("JsonString:\(strToJSON)");
        let data = strToJSON.data(using: String.Encoding.utf8)
        
        var dict : NSDictionary!;
        do {
            
            // dict = try NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers, error: nil) as! Dictionary<String, AnyObject>
            dict = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
            // print(dict)
            return dict;
            
        }
            
        catch let error as NSError {
            print("Error is:\(error)");
            
        }
        
        return dict;
        
    }
    
    static func makePostRequestWithTokenRaw(_ urlString: String, withParameters: String , completion: ((AnyObject?,Bool) -> Void)?)
    {
        let todoEndpoint: String = urlString
        let post :  String = withParameters
        var request = URLRequest(url: URL(string: todoEndpoint)!)
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("Bearer \(ModalController.getTheContentForKey("userToken") as! String)", forHTTPHeaderField: "Authorization")
        request.httpBody = post.data(using: String.Encoding.utf8, allowLossyConversion: true)
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpMethod = "POST"
        
        Alamofire.request(request as URLRequestConvertible).responseJSON { response in
            guard response.result.error == nil else {
                // got an error in getting the data, need to handle it
                
                completion!(nil,false)
                
                print("error calling GET on /todos/1")
                print(response.result.error!)
                return
            }
            
            // make sure we got some JSON since that's what we expect
            guard let json = response.result.value as? [String: Any] else {
                completion!(response as AnyObject,false)
                
                print("didn't get todo object as JSON from API")
                print("Error: \(String(describing: response.result.error))")
                return
            }
            
            completion!(json as AnyObject,true)
            
        }
        
        
    }

}
