//
//  SelectOptionViewController.swift
//  HSE
//
//  Created by Neeraj Tiwari on 10/05/19.
//  Copyright © 2019 Sendan. All rights reserved.
//

import UIKit

class SelectOptionViewController: UIViewController {
    @IBOutlet weak var signUpBtn: UIButton!
  
    @IBOutlet weak var employeeCodeSignUpBtn: UIButton!
    @IBOutlet weak var loginBtn: UIButton!
    @IBOutlet weak var loginBtnLowerLabel: UILabel!
    @IBOutlet weak var signUpLowerLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.signUpLowerLabel.backgroundColor = UIColor(red: 206.0/255.0, green: 120.0/255.0, blue: 81.0/255.0, alpha: 1.0) ;
        self.loginBtnLowerLabel.isHidden = true ;
      self.signUpBtn.isSelected = true
       self.loginBtn.isSelected = false
        
        
        self.employeeCodeSignUpBtn.layer.cornerRadius = self.employeeCodeSignUpBtn.frame.size.height/2
        self.employeeCodeSignUpBtn.clipsToBounds = true
        self.employeeCodeSignUpBtn.layer.borderColor = UIColor(red: 255/255.0, green: 255/255.0, blue: 255/255.0, alpha: 1.0).cgColor
        
        self.employeeCodeSignUpBtn.layer.borderWidth = 1;
        
    }
    @IBAction func signUpBtnClicked(_ sender: Any) {
        self.signUpLowerLabel.backgroundColor =  UIColor(red: 206.0/255.0, green: 120.0/255.0, blue: 81.0/255.0, alpha: 1.0) ;
        self.loginBtnLowerLabel.isHidden = true
        self.signUpLowerLabel.isHidden = false ;
        self.signUpBtn.isSelected = true
        self.loginBtn.isSelected = false
    }

    @IBAction func loginBtnClicked(_ sender: Any) {
       
        self.loginBtnLowerLabel.backgroundColor = UIColor(red: 206.0/255.0, green: 120.0/255.0, blue: 81.0/255.0, alpha: 1.0) ;
        self.signUpLowerLabel.isHidden = true ;
        self.loginBtnLowerLabel.isHidden = false ;
        self.signUpBtn.isSelected = false
        self.loginBtn.isSelected = true
        
        let vc = LoginViewController(nibName: "LoginViewController", bundle: nil)
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    @IBAction func employeeCodeSignBtnClicked(_ sender: Any) {
        let vc = SignUPViewController(nibName: "SignUPViewController", bundle: nil)
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
