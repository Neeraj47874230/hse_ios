//
//  AppDelegate.swift
//  HSE
//
//  Created by Neeraj Tiwari on 10/05/19.
//  Copyright © 2019 Sendan. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import Firebase
import KeychainSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate, MessagingDelegate {

    var window: UIWindow?
    var nav = UINavigationController()


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        // PushNotificationCode
        FirebaseApp.configure()
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()
        Messaging.messaging().delegate = self
        
        let token = Messaging.messaging().fcmToken
        
        // Override point for customization after application launch.
        let keychain = KeychainSwift()
        let dev_id = keychain.get("device_id")
        if dev_id == "" || dev_id == nil {
            let device_id = UIDevice.current.identifierForVendor?.uuidString
            keychain.set("\(device_id!)", forKey: "device_id")
        }
        print("\(keychain.get("device_id"))")
        
        if let ddID = keychain.get("device_id") {
            ModalController.saveTheContent(ddID as AnyObject , WithKey: "deviceID")
        }
        let loginDetail = ModalController.getTheContentForKey("savedUserData")
        
        if !(loginDetail is NSNull) {
            self.window = UIWindow(frame: UIScreen.main.bounds)
            self.window!.backgroundColor = UIColor.white
            let vc = HomeViewController(nibName: "HomeViewController", bundle: nil)
            nav = UINavigationController.init(rootViewController: vc)
            IQKeyboardManager.shared.enable = true
            self.window!.rootViewController = nav
            self.window!.makeKeyAndVisible()
            nav.setNavigationBarHidden(true, animated: false)
            UIApplication.shared.statusBarStyle = .lightContent
            return true
        }else{
        self.window = UIWindow(frame: UIScreen.main.bounds)
        self.window!.backgroundColor = UIColor.white
       let vc = GetStartedViewController(nibName: "GetStartedViewController", bundle: nil)
        nav = UINavigationController.init(rootViewController: vc)
             IQKeyboardManager.shared.enable = true
        self.window!.rootViewController = nav
        self.window!.makeKeyAndVisible()
        nav.setNavigationBarHidden(true, animated: false)
        UIApplication.shared.statusBarStyle = .lightContent
        return true
        }
    }
    func applicationWillResignActive(_ application: UIApplication) {
    }
    func applicationDidEnterBackground(_ application: UIApplication) {
    }
    func applicationWillEnterForeground(_ application: UIApplication) {
    }
    func applicationDidBecomeActive(_ application: UIApplication) {
        UIApplication.shared.applicationIconBadgeNumber = 0
    }
    func applicationWillTerminate(_ application: UIApplication) {
    }
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
        
        let dataDict:[String: String] = ["token": fcmToken]
        NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
        
        ModalController.saveTheContent(fcmToken as AnyObject, WithKey: "firebaseToken")
        
        // TODO: If necessary send token to application server.
        // Note: This callback is fired at each app startup and whenever a new token is generated.
    }
    func application(application: UIApplication,
                     didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        Messaging.messaging().apnsToken = deviceToken
        
        InstanceID.instanceID().instanceID { (result, error) in
            if let error = error {
                print("Error fetching remote instange ID: \(error)")
            } else if let result = result {
                print("Remote instance ID token: \(result.token)")
            }
        }
    }

}

